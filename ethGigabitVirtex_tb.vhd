--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   00:01:09 12/23/2016
-- Design Name:   
-- Module Name:   C:/inzynierka/ethGigabitVirtex/ethGigabitVirtex_tb.vhd
-- Project Name:  ethGigabitVirtex
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: ethGigabitVirtex
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
library ethernet_mac;
USE ieee.std_logic_1164.ALL;
use ethernet_mac.ethernet_types.all;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY ethGigabitVirtex_tb IS
END ethGigabitVirtex_tb;
 
ARCHITECTURE behavior OF ethGigabitVirtex_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT ethGigabitVirtex
	 GENERIC (
		MIIM_DISABLE     : boolean := FALSE
	 );
    PORT(
         clock_100 : IN  std_logic;
         reset_i : IN  std_logic;
         mii_tx_clk_i : IN  std_logic;
         mii_tx_er_o : OUT  std_logic;
         mii_tx_en_o : OUT  std_logic;
         mii_txd_o : OUT  std_ulogic_vector(7 downto 0);
         mii_rx_clk_i : IN  std_logic;
         mii_rx_er_i : IN  std_logic;
         mii_rx_dv_i : IN  std_logic;
         mii_rxd_i : IN  std_ulogic_vector(7 downto 0);
         mdc_o : OUT  std_logic;
         mdio_io : INOUT  std_logic;
         phyrst : OUT  std_logic;
         gmii_gtx_clk_o : OUT  std_logic;
			speed_override_i : IN t_ethernet_speed;
         led_on : OUT  std_logic;
         led_link_up : OUT  std_logic;
         tx_debug_o : OUT  std_ulogic_vector(7 downto 0);
         tx_clock_debug_o : OUT  std_logic;
         rx_debug_o : OUT  std_ulogic_vector(7 downto 0);
         rx_clcok_debug_o : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clock_100 : std_logic := '0';
   signal reset_i : std_logic := '0';
   signal mii_tx_clk_i : std_logic := '0';
   signal mii_rx_clk_i : std_logic := '0';
   signal mii_rx_er_i : std_logic := '0';
   signal mii_rx_dv_i : std_logic := '0';
   signal mii_rxd_i : std_ulogic_vector(7 downto 0) := (others => '0');
	signal speed_override_i     : t_ethernet_speed := SPEED_10MBPS;

	--BiDirs
   signal mdio_io : std_logic;

 	--Outputs
   signal mii_tx_er_o : std_logic;
   signal mii_tx_en_o : std_logic;
   signal mii_txd_o : std_ulogic_vector(7 downto 0);
   signal mdc_o : std_logic;
   signal phyrst : std_logic;
   signal gmii_gtx_clk_o : std_logic;
   signal led_on : std_logic;
   signal led_link_up : std_logic;
   signal tx_debug_o : std_ulogic_vector(7 downto 0);
   signal tx_clock_debug_o : std_logic;
   signal rx_debug_o : std_ulogic_vector(7 downto 0);
   signal rx_clcok_debug_o : std_logic;

   -- Clock period definitions
   constant clock_100_period : time := 10 ns;
	constant mii_clock_period : time := 40 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: ethGigabitVirtex 
	generic map(
			 MIIM_DISABLE => TRUE
	)
	port map (
          clock_100 => clock_100,
          reset_i => reset_i,
          mii_tx_clk_i => mii_tx_clk_i,
          mii_tx_er_o => mii_tx_er_o,
          mii_tx_en_o => mii_tx_en_o,
          mii_txd_o => mii_txd_o,
          mii_rx_clk_i => mii_rx_clk_i,
          mii_rx_er_i => mii_rx_er_i,
          mii_rx_dv_i => mii_rx_dv_i,
          mii_rxd_i => mii_rxd_i,
          mdc_o => mdc_o,
          mdio_io => mdio_io,
          phyrst => phyrst,
          gmii_gtx_clk_o => gmii_gtx_clk_o,
          led_on => led_on,
          led_link_up => led_link_up,
          tx_debug_o => tx_debug_o,
          tx_clock_debug_o => tx_clock_debug_o,
          rx_debug_o => rx_debug_o,
          rx_clcok_debug_o => rx_clcok_debug_o,
			 speed_override_i => speed_override_i
        );

   -- Clock process definitions
   clock_100_process :process
   begin
		clock_100 <= '0';
		wait for clock_100_period/2;
		clock_100 <= '1';
		wait for clock_100_period/2;
   end process;
	
	-- mii_tx_clk_i process definition
	
	mii_tx_clk_i_process : process
	begin
		mii_tx_clk_i <= '0';
		mii_rx_clk_i <= '0';
		wait for mii_clock_period/2;
		mii_tx_clk_i <= '1';
		mii_rx_clk_i <= '1';
		wait for mii_clock_period/2;
	end process;

   -- Stimulus process
   stim_proc: process
   begin	
		-- set speed to 10MBPS
			speed_override_i <= SPEED_10MBPS;
      -- hold reset state for 100 ns.
      wait for 100 ns;	
			reset_i <= '1';
      wait for clock_100_period * 10;
			reset_i <= '0';
		wait for clock_100_period * 100;
      -- insert stimulus here 
		-- preamble(7 octets)
			mii_rxd_i <= "00000101";
		wait for mii_clock_period * 14;
		-- SFD(1 octet)
			mii_rxd_i <= "00000101";
		wait for mii_clock_period;
			mii_rxd_i <= "00001101";
		wait for mii_clock_period;
		-- Destination MAC address(6 octets)
		for i in 0 to 5 loop
				mii_rxd_i <= "00001100";
			wait for mii_clock_period;
				mii_rxd_i <= "00000011";
			wait for mii_clock_period;
		end loop;
		-- Source MAC address(6 octets)
		for i in 0 to 5 loop
				mii_rxd_i <= "00000101";
			wait for mii_clock_period;
				mii_rxd_i <= "00001010";
			wait for mii_clock_period;
		end loop;	
		-- Ethertype(1 octet) (doesn't matter)
			mii_rxd_i <= "00000000";
		wait for mii_clock_period * 2;
		-- Payload (50 octets) 
		for i in 0 to 10 loop
				mii_rxd_i <= "00001010";
			wait for mii_clock_period;
				mii_rxd_i <= "00000101";
			wait for mii_clock_period;
				mii_rxd_i <= "00001100";
			wait for mii_clock_period;
				mii_rxd_i <= "00000011";
			wait for mii_clock_period;
				mii_rxd_i <= "00001110";
			wait for mii_clock_period;
				mii_rxd_i <= "00000111";
			wait for mii_clock_period;
				mii_rxd_i <= "00001111";
			wait for mii_clock_period;
				mii_rxd_i <= "00000000";
			wait for mii_clock_period;
				mii_rxd_i <= "00001111";
			wait for mii_clock_period;
				mii_rxd_i <= "00000000";
			wait for mii_clock_period;
		end loop;
		-- CRC (4 octets) 
		-- generated CRC 11101000001111000110110101101101
			mii_rxd_i <= "00000111";
		wait for mii_clock_period;
			mii_rxd_i <= "00000001";
		wait for mii_clock_period;
			mii_rxd_i <= "00001100";
		wait for mii_clock_period;
			mii_rxd_i <= "00000011";
		wait for mii_clock_period;
			mii_rxd_i <= "00000110";
		wait for mii_clock_period;
			mii_rxd_i <= "00001011";
		wait for mii_clock_period;
			mii_rxd_i <= "00000110";
		wait for mii_clock_period;
			mii_rxd_i <= "00001011";
		wait for mii_clock_period;
      mii_rxd_i <= "00000000";
		end process;

END;
