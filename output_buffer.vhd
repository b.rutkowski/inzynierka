library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity output_buffer is
    Port ( data_in : in  STD_LOGIC;
           clock : in  STD_LOGIC;
           data_out : out  STD_LOGIC);
end output_buffer;

architecture Behavioral of output_buffer is
	-- Force putting input Flip-Flop into IOB so it doesn't end up in a normal logic tile
	-- which would ruin the timing.
	attribute iob : string;
	attribute iob of FDRE_inst : label is "FORCE";
begin
	FDRE_inst : FDRE
	generic map(
		INIT => '0')                -- Initial value of register ('0' or '1')  
	port map(
		Q  => data_out,                -- Data output
		C  => clock,              -- Clock input
		CE => '1',                  -- Clock enable input
		R  => '0',                  -- Synchronous reset input
		D  => data_in              -- Data input
	);

end Behavioral;

