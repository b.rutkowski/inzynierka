/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

#include "xsi.h"

struct XSI_INFO xsi_info;

char *XILINXCORELIB_P_1433929353;
char *IEEE_P_3499444699;
char *IEEE_P_2592010699;
char *STD_STANDARD;
char *XILINXCORELIB_P_3381355550;
char *XILINXCORELIB_P_3106367597;
char *XILINXCORELIB_P_0661866964;
char *STD_TEXTIO;
char *XILINXCORELIB_P_0914797037;
char *IEEE_P_1242562249;
char *XILINXCORELIB_P_0558180590;
char *XILINXCORELIB_P_2147798235;
char *IEEE_P_3620187407;
char *XILINXCORELIB_P_1837083571;
char *XILINXCORELIB_P_0624651749;
char *ETHERNET_MAC_P_0848171809;


int main(int argc, char **argv)
{
    xsi_init_design(argc, argv);
    xsi_register_info(&xsi_info);

    xsi_register_min_prec_unit(-12);
    ieee_p_2592010699_init();
    ieee_p_1242562249_init();
    ethernet_mac_p_0848171809_init();
    work_a_2754114238_3212880686_init();
    std_textio_init();
    xilinxcorelib_p_1837083571_init();
    ieee_p_3499444699_init();
    ieee_p_3620187407_init();
    xilinxcorelib_p_2147798235_init();
    xilinxcorelib_p_0914797037_init();
    xilinxcorelib_p_0558180590_init();
    xilinxcorelib_p_3381355550_init();
    xilinxcorelib_p_1433929353_init();
    xilinxcorelib_p_0661866964_init();
    xilinxcorelib_p_3106367597_init();
    xilinxcorelib_p_0624651749_init();
    xilinxcorelib_a_2511460631_3212880686_init();
    xilinxcorelib_a_0983195614_3212880686_init();
    xilinxcorelib_a_4047887743_3212880686_init();
    xilinxcorelib_a_1615782795_3212880686_init();
    work_a_3739875528_0857466591_init();
    work_a_3970731590_3212880686_init();
    xilinxcorelib_a_2061725686_3212880686_init();
    xilinxcorelib_a_2809251707_3212880686_init();
    xilinxcorelib_a_1564446355_3212880686_init();
    xilinxcorelib_a_3579311745_3212880686_init();
    xilinxcorelib_a_1314797743_3212880686_init();
    work_a_3912861546_1020510680_init();
    xilinxcorelib_a_2196088458_3212880686_init();
    xilinxcorelib_a_2418396324_3212880686_init();
    xilinxcorelib_a_0378608923_3212880686_init();
    xilinxcorelib_a_1474941735_3212880686_init();
    work_a_2460413293_0957269905_init();
    xilinxcorelib_a_2793481480_3212880686_init();
    xilinxcorelib_a_2570771257_3212880686_init();
    xilinxcorelib_a_1134120924_3212880686_init();
    xilinxcorelib_a_0443243899_3212880686_init();
    work_a_4124731617_2303028222_init();
    work_a_0211249688_3212880686_init();
    work_a_1303282778_1768559214_init();
    work_a_3601352010_2372691052_init();


    xsi_register_tops("work_a_3601352010_2372691052");

    XILINXCORELIB_P_1433929353 = xsi_get_engine_memory("xilinxcorelib_p_1433929353");
    IEEE_P_3499444699 = xsi_get_engine_memory("ieee_p_3499444699");
    IEEE_P_2592010699 = xsi_get_engine_memory("ieee_p_2592010699");
    xsi_register_ieee_std_logic_1164(IEEE_P_2592010699);
    STD_STANDARD = xsi_get_engine_memory("std_standard");
    XILINXCORELIB_P_3381355550 = xsi_get_engine_memory("xilinxcorelib_p_3381355550");
    XILINXCORELIB_P_3106367597 = xsi_get_engine_memory("xilinxcorelib_p_3106367597");
    XILINXCORELIB_P_0661866964 = xsi_get_engine_memory("xilinxcorelib_p_0661866964");
    STD_TEXTIO = xsi_get_engine_memory("std_textio");
    XILINXCORELIB_P_0914797037 = xsi_get_engine_memory("xilinxcorelib_p_0914797037");
    IEEE_P_1242562249 = xsi_get_engine_memory("ieee_p_1242562249");
    XILINXCORELIB_P_0558180590 = xsi_get_engine_memory("xilinxcorelib_p_0558180590");
    XILINXCORELIB_P_2147798235 = xsi_get_engine_memory("xilinxcorelib_p_2147798235");
    IEEE_P_3620187407 = xsi_get_engine_memory("ieee_p_3620187407");
    XILINXCORELIB_P_1837083571 = xsi_get_engine_memory("xilinxcorelib_p_1837083571");
    XILINXCORELIB_P_0624651749 = xsi_get_engine_memory("xilinxcorelib_p_0624651749");
    ETHERNET_MAC_P_0848171809 = xsi_get_engine_memory("ethernet_mac_p_0848171809");

    return xsi_run_simulation(argc, argv);

}
