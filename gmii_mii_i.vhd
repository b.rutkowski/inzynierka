library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library ethernet_mac;
use ethernet_mac.ethernet_types.all;
library UNISIM;
use UNISIM.VComponents.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity gmii_mii_i is
    Port ( 
		  -- 200 MHz clock for IODELAY
		  clock_200 : in STD_LOGIC;
		  
		  -- reset
		  reset_i : in STD_LOGIC;
		  
		  -- signals connected to MII externals
		  mii_rx_clk_i : in  STD_LOGIC;
		  mii_rx_dv_i : in  STD_LOGIC;
		  mii_rx_er_i : in  STD_LOGIC;
		  mii_rxd_i : in  t_ethernet_data;
		  
		  -- Signals connected to the mii_gmii module
		  int_mii_rx_er_o : out std_ulogic;
		  int_mii_rx_dv_o : out std_ulogic;
		  int_mii_rxd_o   : out t_ethernet_data;
		  
		  -- clock output
		  clock_rx_o : out std_ulogic);
end gmii_mii_i;

architecture Behavioral of gmii_mii_i is
	signal clock_rx : STD_LOGIC;
	signal idealayctrl_rdy: STD_LOGIC;

begin
	IDELAYCTRL_inst : IDELAYCTRL
	port map (
		RDY => idealayctrl_rdy, -- 1-bit output indicates validity of the REFCLK
		REFCLK => clock_200, -- 1-bit reference clock input
		RST => reset_i -- 1-bit reset input
	);
	
	mii_rx_clk_buffer_inst : entity work.mii_rx_clk_buffer
	port map(
	   mii_rx_clk_i => mii_rx_clk_i,
	   clock_rx_o => clock_rx
	);
	
	mii_rx_er_buffer_inst : entity work.input_buffer
	port map (
		data_in => mii_rx_er_i,
	   clock => clock_rx,
	   data_out => int_mii_rx_er_o
	);
	
	mii_rx_dv_buffer_inst : entity work.input_buffer
	port map (
		data_in => mii_rx_dv_i,
	   clock => clock_rx,
	   data_out => int_mii_rx_dv_o
	);
	
	mii_rxd_buffer_generate : for i in mii_rxd_i'range generate
		mii_rxd_buffer_inst : entity work.input_buffer
			port map(
				data_in => mii_rxd_i(i),
				clock => clock_rx,
				data_out => int_mii_rxd_o(i)
			);
	end generate;
	
	clock_rx_o <= clock_rx;
	
end Behavioral;

