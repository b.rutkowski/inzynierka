# This file is a general .ucf for Genesys rev C board
# To use it in a project:
# - remove or comment the lines corresponding to unused pins
# - rename the used signals according to the project

# clock pin for Genesys rev C board
NET "clock_100" LOC = AG18;
NET "clock_100" IOSTANDARD = LVCMOS33;
# Bank = 4, Pin name = IO_L6P_GC_4, Type = GCLK, Sch name = GCLK0

# onBoard SWITCHES
# Bank = 3, Pin name = IO_L3N_GC_3,      Sch name = SW0
NET "reset_i" LOC = J19;
NET "reset_i" IOSTANDARD = LVCMOS25;
NET "reset_i" DRIVE = 2;
NET "reset_i" SLEW = SLOW;
 
# TEMAC Ethernet MAC
# Bank = 12, Pin name = IO_L4P_12,      Sch name = E-RESET  
NET "phyrst" LOC = L4;

# Bank = 3,  Pin name = IO_L4P_GC_3,    Sch name = E-TXCLK 
NET "mii_tx_clk_i" LOC = J16;
NET "mii_tx_clk_i" IOSTANDARD = LVCMOS25;

# Bank = 12, Pin name = IO_L8N_CC_12,   Sch name = E-TXD0
NET "mii_txd_o[0]" LOC = J5;
NET "mii_txd_o[0]" IOSTANDARD = LVCMOS25;
# Bank = 12, Pin name = IO_L13N_12,     Sch name = E-TXD1
NET "mii_txd_o[1]" LOC = G5;
NET "mii_txd_o[1]" IOSTANDARD = LVCMOS25;
# Bank = 12, Pin name = IO_L15P_12,     Sch name = E-TXD2
NET "mii_txd_o[2]" LOC = F5;
NET "mii_txd_o[2]" IOSTANDARD = LVCMOS25;
# Bank = 12, Pin name = IO_L9P_CC_12,   Sch name = E-TXD3
NET "mii_txd_o[3]" LOC = R7;
NET "mii_txd_o[3]" IOSTANDARD = LVCMOS25;
# Bank = 12, Pin name = IO_L10P_CC_12,  Sch name = E-TXD4
NET "mii_txd_o[4]" LOC = T8;
NET "mii_txd_o[4]" IOSTANDARD = LVCMOS25;
# Bank = 12, Pin name = IO_L14P_12,     Sch name = E-TXD5
NET "mii_txd_o[5]" LOC = R11;
NET "mii_txd_o[5]" IOSTANDARD = LVCMOS25;
# Bank = 12, Pin name = IO_L16N_12,     Sch name = E-TXD6
NET "mii_txd_o[6]" LOC = T11;
NET "mii_txd_o[6]" IOSTANDARD = LVCMOS25;
# Bank = 12, Pin name = IO_L10N_CC_12,  Sch name = E-TXD7
NET "mii_txd_o[7]" LOC = U7;
NET "mii_txd_o[7]" IOSTANDARD = LVCMOS25;

# Bank = 12, Pin name = IO_L16P_12,     Sch name = E-TXEN
NET "mii_tx_en_o" LOC = T10;
NET "mii_tx_en_o" IOSTANDARD = LVDCI_25;
# Bank = 12, Pin name = IO_L9N_CC_12,   Sch name = E-TXER
NET "mii_tx_er_o" LOC = R8;
NET "mii_tx_er_o" IOSTANDARD = LVDCI_25;
# Bank = 3,  Pin name = IO_L7P_GC_3,    Sch name = E-GTXCLK
NET "gmii_gtx_clk_o" LOC = J20;
NET "gmii_gtx_clk_o" IOSTANDARD = LVCMOS25;

# Bank = 12, Pin name = IO_L1N_12,      Sch name = E-RXD0
NET "mii_rxd_i[0]" LOC = N7;
NET "mii_rxd_i[0]" IOSTANDARD = LVCMOS25;
# Bank = 12, Pin name = IO_L7P_12,      Sch name = E-RXD1
NET "mii_rxd_i[1]" LOC = R6;
NET "mii_rxd_i[1]" IOSTANDARD = LVCMOS25;
# Bank = 12, Pin name = IO_L5N_12,      Sch name = E-RXD2
NET "mii_rxd_i[2]" LOC = P6;
NET "mii_rxd_i[2]" IOSTANDARD = LVCMOS25;
# Bank = 12, Pin name = IO_L3N_12,      Sch name = E-RXD3
NET "mii_rxd_i[3]" LOC = P5;
NET "mii_rxd_i[3]" IOSTANDARD = LVCMOS25;
# Bank = 12, Pin name = IO_L2P_12,      Sch name = E-RXD4
NET "mii_rxd_i[4]" LOC = M7;
NET "mii_rxd_i[4]" IOSTANDARD = LVCMOS25;
# Bank = 12, Pin name = IO_L0P_12,      Sch name = E-RXD5
NET "mii_rxd_i[5]" LOC = M6;
NET "mii_rxd_i[5]" IOSTANDARD = LVCMOS25;
# Bank = 12, Pin name = IO_L0N_12,      Sch name = E-RXD6
NET "mii_rxd_i[6]" LOC = M5;
NET "mii_rxd_i[6]" IOSTANDARD = LVCMOS25;
# Bank = 12, Pin name = IO_L2N_12,      Sch name = E-RDX7
NET "mii_rxd_i[7]" LOC = L6;
NET "mii_rxd_i[7]" IOSTANDARD = LVCMOS25;

# Bank = 12, Pin name = IO_L1P_12,      Sch name = E-RXDV
NET "mii_rx_dv_i" LOC = N8;
NET "mii_rx_dv_i" IOSTANDARD = LVCMOS25;
# Bank = 12, Pin name = IO_L5P_12,      Sch name = E-RXER
NET "mii_rx_er_i" LOC = P7;
NET "mii_rx_er_i" IOSTANDARD = LVCMOS25;
# Bank = 3,  Pin name = IO_L5P_GC_3,    Sch name = E-RXCLK
NET "mii_rx_clk_i" LOC = L19;
NET "mii_rx_clk_i" IOSTANDARD = LVCMOS25;
# Bank = 12, Pin name = IO_L3P_12,      Sch name = E-MDC
NET "mdc_o" LOC = N5;
NET "mdc_o" IOSTANDARD = LVCMOS25;
# Bank = 12, Pin name = IO_L18N_12,     Sch name = E-MDIO
NET "mdio_io" LOC = U10;
NET "mdio_io" IOSTANDARD = LVCMOS25;
#NET "phyint"     LOC = "T6";   # Bank = 12, Pin name = IO_L7N_12,      Sch name = E-INT
 
NET "rx_debug_o[1]" LOC = AD9;
NET "rx_debug_o[0]" LOC = AD11;
NET "rx_debug_o[2]" LOC = AM13;
NET "rx_debug_o[3]" LOC = AM12;
NET "rx_debug_o[4]" LOC = AD10;
NET "rx_debug_o[5]" LOC = AE8;
NET "rx_debug_o[6]" LOC = AF10;
NET "rx_debug_o[7]" LOC = AJ11;
NET "tx_debug_o[0]" LOC = AE9;
NET "tx_debug_o[1]" LOC = AC8;
NET "tx_debug_o[2]" LOC = AB10;
NET "tx_debug_o[3]" LOC = AC9;
NET "tx_debug_o[4]" LOC = AF8;
NET "tx_debug_o[5]" LOC = AB8;
NET "tx_debug_o[6]" LOC = AA10;
NET "tx_debug_o[7]" LOC = AA9;

# PlanAhead Generated IO constraints 

NET "rx_debug_o[7]" IOSTANDARD = LVCMOS25;
NET "rx_debug_o[6]" IOSTANDARD = LVCMOS25;
NET "rx_debug_o[5]" IOSTANDARD = LVCMOS25;
NET "rx_debug_o[4]" IOSTANDARD = LVCMOS25;
NET "rx_debug_o[3]" IOSTANDARD = LVCMOS25;
NET "rx_debug_o[2]" IOSTANDARD = LVCMOS25;
NET "rx_debug_o[1]" IOSTANDARD = LVCMOS25;
NET "rx_debug_o[0]" IOSTANDARD = LVCMOS25;
NET "tx_debug_o[7]" IOSTANDARD = LVCMOS25;
NET "tx_debug_o[6]" IOSTANDARD = LVCMOS25;
NET "tx_debug_o[5]" IOSTANDARD = LVCMOS25;
NET "tx_debug_o[4]" IOSTANDARD = LVCMOS25;
NET "tx_debug_o[3]" IOSTANDARD = LVCMOS25;
NET "tx_debug_o[2]" IOSTANDARD = LVCMOS25;
NET "tx_debug_o[1]" IOSTANDARD = LVCMOS25;
NET "tx_debug_o[0]" IOSTANDARD = LVCMOS25;
