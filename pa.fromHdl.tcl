
# PlanAhead Launch Script for Pre-Synthesis Floorplanning, created by Project Navigator

create_project -name ethGigabitVirtex -dir "C:/inzynierka/ethGigabitVirtex/planAhead_run_3" -part xc5vlx50tff1136-2
set_param project.pinAheadLayout yes
set srcset [get_property srcset [current_run -impl]]
set_property target_constrs_file "ethGigabitVirtex.ucf" [current_fileset -constrset]
set hdlfile [add_files [list {ethernet_mac/utility.vhd}]]
set_property file_type VHDL $hdlfile
set_property library ethernet_mac $hdlfile
set hdlfile [add_files [list {ethernet_mac/ethernet_types.vhd}]]
set_property file_type VHDL $hdlfile
set_property library ethernet_mac $hdlfile
set hdlfile [add_files [list {ethernet_mac/crc.vhd}]]
set_property file_type VHDL $hdlfile
set_property library ethernet_mac $hdlfile
set hdlfile [add_files [list {tx_clk_gen.vhd}]]
set_property file_type VHDL $hdlfile
set_property library ethernet_mac $hdlfile
set hdlfile [add_files [list {output_buffer.vhd}]]
set_property file_type VHDL $hdlfile
set_property library ethernet_mac $hdlfile
set hdlfile [add_files [list {mii_rx_clk_buffer.vhd}]]
set_property file_type VHDL $hdlfile
set_property library ethernet_mac $hdlfile
set hdlfile [add_files [list {input_buffer.vhd}]]
set_property file_type VHDL $hdlfile
set_property library ethernet_mac $hdlfile
set hdlfile [add_files [list {ethernet_mac/miim_types.vhd}]]
set_property file_type VHDL $hdlfile
set_property library ethernet_mac $hdlfile
set hdlfile [add_files [list {ethernet_mac/crc32.vhd}]]
set_property file_type VHDL $hdlfile
set_property library ethernet_mac $hdlfile
set hdlfile [add_files [list {mii_gmii_io.vhd}]]
set_property file_type VHDL $hdlfile
set_property library ethernet_mac $hdlfile
set hdlfile [add_files [list {gmii_mii_tx.vhd}]]
set_property file_type VHDL $hdlfile
set_property library ethernet_mac $hdlfile
set hdlfile [add_files [list {gmii_mii_i.vhd}]]
set_property file_type VHDL $hdlfile
set_property library ethernet_mac $hdlfile
set hdlfile [add_files [list {ethernet_mac/single_signal_synchronizer.vhd}]]
set_property file_type VHDL $hdlfile
set_property library ethernet_mac $hdlfile
set hdlfile [add_files [list {ethernet_mac/miim_registers.vhd}]]
set_property file_type VHDL $hdlfile
set_property library ethernet_mac $hdlfile
set hdlfile [add_files [list {ethernet_mac/framing_common.vhd}]]
set_property file_type VHDL $hdlfile
set_property library ethernet_mac $hdlfile
set hdlfile [add_files [list {mii_gmii_io_virtex5.vhd}]]
set_property file_type VHDL $hdlfile
set_property library ethernet_mac $hdlfile
set hdlfile [add_files [list {ipcore_dir/ethernet_mac_tx_fifo_xilinx.vhd}]]
set_property file_type VHDL $hdlfile
set_property library ethernet_mac $hdlfile
set hdlfile [add_files [list {ethernet_mac/xilinx/single_signal_synchronizer_spartan6.vhd}]]
set_property file_type VHDL $hdlfile
set_property library ethernet_mac $hdlfile
set hdlfile [add_files [list {ethernet_mac/tx_fifo_adapter.vhd}]]
set_property file_type VHDL $hdlfile
set_property library ethernet_mac $hdlfile
set hdlfile [add_files [list {ethernet_mac/reset_generator.vhd}]]
set_property file_type VHDL $hdlfile
set_property library ethernet_mac $hdlfile
set hdlfile [add_files [list {ethernet_mac/mii_gmii.vhd}]]
set_property file_type VHDL $hdlfile
set_property library ethernet_mac $hdlfile
set hdlfile [add_files [list {ethernet_mac/miim_control.vhd}]]
set_property file_type VHDL $hdlfile
set_property library ethernet_mac $hdlfile
set hdlfile [add_files [list {ethernet_mac/miim.vhd}]]
set_property file_type VHDL $hdlfile
set_property library ethernet_mac $hdlfile
set hdlfile [add_files [list {ethernet_mac/framing.vhd}]]
set_property file_type VHDL $hdlfile
set_property library ethernet_mac $hdlfile
set hdlfile [add_files [list {ethernet_mac/xilinx/tx_fifo.vhd}]]
set_property file_type VHDL $hdlfile
set_property library ethernet_mac $hdlfile
set hdlfile [add_files [list {ethernet_mac/rx_fifo.vhd}]]
set_property file_type VHDL $hdlfile
set_property library ethernet_mac $hdlfile
set hdlfile [add_files [list {ethernet_mac/ethernet.vhd}]]
set_property file_type VHDL $hdlfile
set_property library ethernet_mac $hdlfile
set hdlfile [add_files [list {loopback.vhd}]]
set_property file_type VHDL $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {pll.vhd}]]
set_property file_type VHDL $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {ethernet_mac/ethernet_with_fifos.vhd}]]
set_property file_type VHDL $hdlfile
set_property library ethernet_mac $hdlfile
set hdlfile [add_files [list {ethGigabitVirtex.vhd}]]
set_property file_type VHDL $hdlfile
set_property library work $hdlfile
set_property top ethGigabitVirtex $srcset
add_files [list {ethGigabitVirtex.ucf}] -fileset [get_property constrset [current_run]]
add_files [list {ipcore_dir/ethernet_mac_tx_fifo_xilinx.ncf}] -fileset [get_property constrset [current_run]]
open_rtl_design -part xc5vlx50tff1136-2
