--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   20:32:09 12/21/2016
-- Design Name:   
-- Module Name:   C:/inzynierka/ethGigabitVirtex/loopback_tb.vhd
-- Project Name:  ethGigabitVirtex
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: loopback
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY loopback_tb IS
END loopback_tb;
 
ARCHITECTURE behavior OF loopback_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT loopback
    PORT(
         clock : IN  std_logic;
         reset : IN  std_logic;
         rx_empty : IN  std_logic;
         rx_data : IN  std_logic_vector(7 downto 0);
         tx_data : OUT  std_logic_vector(7 downto 0);
         tx_wr_en : OUT  std_logic;
         rx_rd_en : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clock : std_logic := '0';
   signal reset : std_logic := '0';
   signal rx_empty : std_logic := '0';
   signal rx_data : std_logic_vector(7 downto 0) := (others => '0');

 	--Outputs
   signal tx_data : std_logic_vector(7 downto 0);
   signal tx_wr_en : std_logic;
   signal rx_rd_en : std_logic;

   -- Clock period definitions
   constant clock_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: loopback PORT MAP (
          clock => clock,
          reset => reset,
          rx_empty => rx_empty,
          rx_data => rx_data,
          tx_data => tx_data,
          tx_wr_en => tx_wr_en,
          rx_rd_en => rx_rd_en
        );

   -- Clock process definitions
   clock_process :process
   begin
		clock <= '0';
		wait for clock_period/2;
		clock <= '1';
		wait for clock_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for clock_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;
