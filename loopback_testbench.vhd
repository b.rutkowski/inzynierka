--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   21:24:41 04/04/2017
-- Design Name:   
-- Module Name:   C:/inzynierka/ethGigabitVirtex/loopback_testbench.vhd
-- Project Name:  ethGigabitVirtex
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: loopback_register
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY loopback_testbench IS
END loopback_testbench;
 
ARCHITECTURE behavior OF loopback_testbench IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
	COMPONENT loopback 
	PORT (
		clk: in std_logic;
		reset_i: in STD_LOGIC;
		rx_empty_i: in std_logic;
		tx_wr_en_o: out std_logic;
		rx_data_i: in STD_LOGIC_VECTOR(7 downto 0);
		tx_data_o: out STD_LOGIC_VECTOR(7 downto 0)
	);
	END COMPONENT;

   --Inputs
   signal clk : std_logic := '0';
   signal rx_data_i : std_logic_vector(7 downto 0) := (others => '0');
	signal tx_data_o: std_logic_vector(7 downto 0) := (others => '0');
	signal rx_empty_i : std_logic := '1';
   signal write_en_i : std_logic := '0';
	signal reset_i : std_logic := '0';

 	--Outputs
   signal data_o : std_logic_vector(7 downto 0);
	signal tx_wr_en_o: std_logic := '0';

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: loopback PORT MAP (
          clk => clk,
			 reset_i => reset_i,
          rx_empty_i => rx_empty_i,
			 tx_wr_en_o => tx_wr_en_o,
          rx_data_i => rx_data_i,
          tx_data_o => tx_data_o
   );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
		reset_i <= '1';
      wait for 100 ns;	
		reset_i <= '0';
      wait for clk_period*10;

      -- insert stimulus here 
		rx_empty_i <= '0';
		rx_data_i <= "11111101";
		wait for clk_period;
		rx_data_i <= "11111011";
		wait for clk_period;
		rx_data_i <= "11110111";
		wait for clk_period;
		rx_data_i <= "11101111";
		wait for clk_period;
		rx_empty_i <= '1';
		rx_data_i <= "00000000";
		wait for clk_period * 12;
		
--		rx_empty_i <= '0';
--		rx_data_i <= "10101010";
--		wait for clk_period * 4;
--		rx_data_i <= "01010101";
--		wait for clk_period * 4;
--		rx_empty_i <= '1';
--		wait for clk_period * 8;
--		reset_i <= '1';
--      wait for clk_period * 2;
--		reset_i <= '0';
		wait;
   end process;

END;
