
# PlanAhead Launch Script for Post-Synthesis floorplanning, created by Project Navigator

create_project -name ethGigabitVirtex -dir "C:/inzynierka/ethGigabitVirtex/planAhead_run_4" -part xc5vlx50tff1136-2
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "C:/inzynierka/ethGigabitVirtex/ethGigabitVirtex.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {C:/inzynierka/ethGigabitVirtex} {ipcore_dir} }
add_files [list {ipcore_dir/ethernet_mac_tx_fifo_xilinx.ncf}] -fileset [get_property constrset [current_run]]
set_property target_constrs_file "ethGigabitVirtex.ucf" [current_fileset -constrset]
add_files [list {ethGigabitVirtex.ucf}] -fileset [get_property constrset [current_run]]
link_design
