#ChipScope Core Inserter Project File Version 3.0
#Tue Dec 20 14:05:06 CET 2016
Project.device.designInputFile=C\:\\inzynierka\\ethGigabitVirtex\\ethGigabitVirtex_cs.ngc
Project.device.designOutputFile=C\:\\inzynierka\\ethGigabitVirtex\\ethGigabitVirtex_cs.ngc
Project.device.deviceFamily=14
Project.device.enableRPMs=true
Project.device.outputDirectory=C\:\\inzynierka\\ethGigabitVirtex\\_ngo
Project.device.useSRL16=true
Project.filter.dimension=4
Project.filter<0>=
Project.filter<1>=cl
Project.filter<2>=clk
Project.filter<3>=clock
Project.icon.boundaryScanChain=1
Project.icon.enableExtTriggerIn=false
Project.icon.enableExtTriggerOut=false
Project.icon.triggerInPinName=
Project.icon.triggerOutPinName=
Project.unit.dimension=1
Project.unit<0>.clockChannel=clock_200
Project.unit<0>.clockEdge=Rising
Project.unit<0>.dataChannel<0>=clock_125
Project.unit<0>.dataChannel<1>=mii_tx_clk_i_IBUFG
Project.unit<0>.dataChannel<2>=mii_rxd_i_2_IBUF
Project.unit<0>.dataChannel<3>=mii_rx_dv_i_IBUF
Project.unit<0>.dataChannel<4>=mii_rx_er_i_IBUF
Project.unit<0>.dataChannel<5>=reset_i_IBUF
Project.unit<0>.dataChannel<6>=mii_rxd_i_0_IBUF
Project.unit<0>.dataChannel<7>=mii_rxd_i_1_IBUF
Project.unit<0>.dataDepth=1024
Project.unit<0>.dataEqualsTrigger=false
Project.unit<0>.dataPortWidth=8
Project.unit<0>.enableGaps=false
Project.unit<0>.enableStorageQualification=true
Project.unit<0>.enableTimestamps=false
Project.unit<0>.timestampDepth=0
Project.unit<0>.timestampWidth=0
Project.unit<0>.triggerChannel<0><0>=clock_200
Project.unit<0>.triggerConditionCountWidth=0
Project.unit<0>.triggerMatchCount<0>=1
Project.unit<0>.triggerMatchCountWidth<0><0>=0
Project.unit<0>.triggerMatchType<0><0>=5
Project.unit<0>.triggerPortCount=1
Project.unit<0>.triggerPortIsData<0>=true
Project.unit<0>.triggerPortWidth<0>=1
Project.unit<0>.triggerSequencerLevels=16
Project.unit<0>.triggerSequencerType=1
Project.unit<0>.type=ilapro
