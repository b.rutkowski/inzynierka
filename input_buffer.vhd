library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity input_buffer is
    Port ( data_in : in  STD_LOGIC;
			  clock : in  STD_LOGIC;
           data_out : out  STD_LOGIC);
end input_buffer;

architecture Behavioral of input_buffer is
	-- Force putting input Flip-Flop into IOB so it doesn't end up in a normal logic tile
	-- which would ruin the timing.
	attribute iob : string;
	attribute iob of FDRE_inst : label is "FORCE";
	
	signal idealayctrl_rdy: STD_LOGIC;
	signal delayed_data: STD_LOGIC;

begin
	IODELAY_inst : IODELAY
	generic map (
		DELAY_SRC => "I", -- Specify which input port to be used "I"=IDATAIN, "O"=ODATAIN, "DATAIN"=DATAIN, "IO"=Bi-directional
		HIGH_PERFORMANCE_MODE => TRUE, -- TRUE specifies lower jitter at expense of more power
		IDELAY_TYPE => "FIXED", -- "DEFAULT", "FIXED" or "VARIABLE"
		IDELAY_VALUE => 0, -- 0 to 63 tap values
		ODELAY_VALUE => 0, -- 0 to 63 tap values
		REFCLK_FREQUENCY => 200.0, -- Frequency used for IDELAYCTRL 175.0 to 225.0
		SIGNAL_PATTERN => "DATA") -- Input signal type, "CLOCK" or "DATA"
	port map (
		DATAOUT => delayed_data, -- 1-bit delayed data output
		C => '0', -- 1-bit clock input
		CE => '0', -- 1-bit clock enable input
		DATAIN => '0', -- 1-bit internal data input ??
		IDATAIN => data_in, -- 1-bit input data input (connect to port) ??
		INC => '0', -- 1-bit increment/decrement input
		ODATAIN => '0', -- 1-bit output data input
		RST => '0', -- 1-bit active high, synch reset input
		T => '1' -- 1-bit 3-state control input
	);
	
	FDRE_inst : FDRE
		generic map(
			INIT => '0')                -- Initial value of register ('0' or '1')  
		port map(
			Q  => data_out,             -- Data output
			C  => clock,              -- Clock input
			CE => '1',                  -- Clock enable input
			R  => '0',                  -- Synchronous reset input
			D  => delayed_data               -- Data input
		);


end Behavioral;

