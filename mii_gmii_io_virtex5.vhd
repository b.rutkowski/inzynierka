library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

architecture virtex5 of mii_gmii_io is

begin

	gmii_mii_i_inst : entity work.gmii_mii_i
	port map(
		  clock_200 => clock_200,
		  reset_i => '0', -------------------
		  mii_rx_clk_i => mii_rx_clk_i,
		  mii_rx_dv_i => mii_rx_dv_i,
		  mii_rx_er_i => mii_rx_er_i,
		  mii_rxd_i => mii_rxd_i,
		  int_mii_rx_er_o => int_mii_rx_er_o,
		  int_mii_rx_dv_o => int_mii_rx_dv_o,
		  int_mii_rxd_o => int_mii_rxd_o,
		  clock_rx_o => clock_rx_o
	);
	
	gmii_mii_o_inst : entity work.gmii_mii_o
	port map(
		  clock_125_i => clock_125_i,
		  mii_tx_er_o => open, ------------------------- ??
		  mii_tx_en_o => mii_tx_en_o,
		  mii_tx_clk_i => mii_tx_clk_i,
		  mii_txd_o => mii_txd_o,
		  gmii_gtx_clk_o => gmii_gtx_clk_o,
		  speed_select_i => speed_select_i,
		  int_mii_tx_er_i => '0', -------------------- ??
		  int_mii_tx_en_i => int_mii_tx_en_i,
		  int_mii_txd_i => int_mii_txd_i,
		  clock_tx_o => clock_tx_o
	);
end virtex5;

