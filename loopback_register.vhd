----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:11:59 04/10/2017 
-- Design Name: 
-- Module Name:    loopback_register - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity loopback_register is
    Port ( 
			  clk : in  STD_LOGIC;
			  reset_i : in  STD_LOGIC;
			  read_en_i : in  STD_LOGIC;
           data_i : in  STD_LOGIC_VECTOR (7 downto 0);
           write_en_i : in  STD_LOGIC;
           data_o : out  STD_LOGIC_VECTOR (7 downto 0));
end loopback_register;

architecture Behavioral of loopback_register is

begin
	data_fifo_inst : entity work.data_fifo
		port map(
			clk           => clk,
			rst           => reset_i,
			din           => data_i,
			wr_en         => write_en_i,
			rd_en         => read_en_i,
			dout          => data_o
		);
end Behavioral;

