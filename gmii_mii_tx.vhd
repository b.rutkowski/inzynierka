library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library ethernet_mac;
use ethernet_mac.ethernet_types.all;
library UNISIM;
use UNISIM.VComponents.all;

entity gmii_mii_o is
    Port ( 
		  clock_125_i : in  STD_LOGIC; -- 125 MHz clock input (exact requirements can vary by implementation)
		  
		  -- signals connected to MII externals
		  mii_tx_er_o : out  STD_LOGIC;
		  mii_tx_en_o : out  STD_LOGIC;
		  mii_tx_clk_i : in  STD_LOGIC;
		  mii_txd_o : out  t_ethernet_data;
		  
		  --GMII out clock
		  gmii_gtx_clk_o : out STD_LOGIC;
		  
		  --Speed selector
		  speed_select_i : in t_ethernet_speed;
		  
		  -- Signals connected to the mii_gmii module
		  int_mii_tx_er_i : in  std_ulogic;
		  int_mii_tx_en_i : in  std_ulogic;
		  int_mii_txd_i   : in  t_ethernet_data;
		  
		  -- Output clock
		  clock_tx_o : out STD_ULOGIC);
end gmii_mii_o;

architecture Behavioral of gmii_mii_o is
	signal clock_tx : STD_LOGIC;
	signal gmii_active : STD_LOGIC;
	
begin
	with speed_select_i select gmii_active <=
		'1' when SPEED_1000MBPS,
		'0' when others;
		
	tx_clk_gen_inst : entity work.tx_clk_gen
	port map(
		clock_125_i => clock_125_i,
	   mii_tx_clk_i => mii_tx_clk_i,
		speed_select_i => speed_select_i,
		clock_tx_o => clock_tx 
	);
	
	ODDR_inst : ODDR
	generic map(
		DDR_CLK_EDGE => "OPPOSITE_EDGE", -- "OPPOSITE_EDGE" or "SAME_EDGE"
		INIT => '0', -- Initial value for Q port ('1' or '0')
		SRTYPE => "SYNC") -- Reset Type ("ASYNC" or "SYNC")
	port map (
		Q => gmii_gtx_clk_o, -- 1-bit DDR output
		C => clock_tx, -- 1-bit clock input
		CE => gmii_active, -- 1-bit clock enable input
		D1 => '0', -- 1-bit data input (positive edge) ??
		D2 => '1', -- 1-bit data input (negative edge) ??
		R => '0', -- 1-bit reset input
		S => '0' -- 1-bit set input
	);
	
	tx_en_buffer_inst : entity work.output_buffer
	port map(
		data_in => int_mii_tx_en_i,
		clock => clock_tx,
      data_out => mii_tx_en_o
	);
	
	tx_er_buffer_inst : entity work.output_buffer
	port map(
		data_in => int_mii_tx_er_i,
		clock => clock_tx,
      data_out => mii_tx_er_o
	);
	
	mii_txd_buffer_generate : for i in mii_txd_o'range generate
		mii_txd_buffer_inst : entity work.output_buffer
			port map(
				data_in => int_mii_txd_i(i),
				clock => clock_tx,
				data_out => mii_txd_o(i)
			);
	end generate;
	
	clock_tx_o <= clock_tx;

end Behavioral;

