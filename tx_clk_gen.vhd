library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VComponents.all;
library ethernet_mac;
use ethernet_mac.ethernet_types.all;

entity tx_clk_gen is
    Port ( clock_125_i : in  STD_LOGIC;
           mii_tx_clk_i : in  STD_LOGIC;
			  speed_select_i  : in  t_ethernet_speed;
           clock_tx_o : out  STD_LOGIC);
end tx_clk_gen;

architecture Behavioral of tx_clk_gen is
	signal gmii_active : STD_LOGIC;
	
begin
	with speed_select_i select gmii_active <=
		'1' when SPEED_1000MBPS,
		'0' when others;
	clock_tx_BUFGMUX_inst : BUFGMUX
		generic map(
			CLK_SEL_TYPE => "ASYNC"     -- Glitchles ("SYNC") or fast ("ASYNC") clock switch-over
		)
		port map(
			O  => clock_tx_o,             -- 1-bit output: Clock buffer output
			I0 => mii_tx_clk_i,         -- 1-bit input: Clock buffer input (S=0)
			I1 => clock_125_i,          -- 1-bit input: Clock buffer input (S=1)
			S  => gmii_active           -- 1-bit input: Clock buffer select
		);
end Behavioral;

