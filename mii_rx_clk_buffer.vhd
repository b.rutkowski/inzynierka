library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity mii_rx_clk_buffer is
    Port ( 
		  mii_rx_clk_i : in  STD_LOGIC;
		  clock_rx_o : out  STD_LOGIC);
end mii_rx_clk_buffer;

architecture Behavioral of mii_rx_clk_buffer is
	signal delayed_clock : STD_LOGIC;
	
begin	
	IODELAY_inst : IODELAY
	generic map (
		DELAY_SRC => "I", -- Specify which input port to be used "I"=IDATAIN, "O"=ODATAIN, "DATAIN"=DATAIN, "IO"=Bi-directional
		HIGH_PERFORMANCE_MODE => TRUE, -- TRUE specifies lower jitter at expense of more power
		IDELAY_TYPE => "FIXED", -- "DEFAULT", "FIXED" or "VARIABLE"
		IDELAY_VALUE => 0, -- 0 to 63 tap values
		ODELAY_VALUE => 0, -- 0 to 63 tap values
		REFCLK_FREQUENCY => 200.0, -- Frequency used for IDELAYCTRL 175.0 to 225.0
		SIGNAL_PATTERN => "CLOCK") -- Input signal type, "CLOCK" or "DATA"
	port map (
		DATAOUT => delayed_clock, -- 1-bit delayed data output
		C => '0', -- 1-bit clock input
		CE => '0', -- 1-bit clock enable input
		DATAIN => '0', -- 1-bit internal data input ??
		IDATAIN => mii_rx_clk_i, -- 1-bit input data input (connect to port) ??
		INC => '0', -- 1-bit increment/decrement input
		ODATAIN => '0', -- 1-bit output data input
		RST => '0', -- 1-bit active high, synch reset input
		T => '1' -- 1-bit 3-state control input
	);
	
	BUFG_inst : BUFG
	port map (
		O => clock_rx_o,
		I => delayed_clock
	);

end Behavioral;

